package com.xm.qa.testcases;

import com.xm.qa.base.TestBase;
import com.xm.qa.pages.HomePage;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class HomePageTest extends TestBase {
    HomePage homePage;

    public HomePageTest(){
        super();
    }

    @BeforeMethod
    public void setUp(){
        initialization(prop.getProperty("urlHomePage"));
        homePage = new HomePage();
    }
    @Test(priority = 1)
    public void homePageTitleTest(){
        String actualTitle = homePage.validateHomePageTitle();
        Assert.assertEquals(actualTitle, "Forex & CFD Trading on Stocks, Indices, Oil, Gold by XM™");
    }
    @Test(priority = 2)
    public void homePageLogoTest() throws InterruptedException {
        boolean flag = homePage.validateHomePageLogo();
        Assert.assertEquals(flag, true);
    }

    @Test(priority = 4)
    public void researchAndEducationMenuTest(){
        boolean researchAndEducationMenuText = homePage.researchAndEducationTextIsDisplayed();
        Assert.assertEquals(researchAndEducationMenuText, true);
    }

    @AfterMethod
    public void tearDown(){
        driver.close();
        driver.quit();
    }
}
