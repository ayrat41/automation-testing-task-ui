package com.xm.qa.testcases;

import com.xm.qa.base.TestBase;
import com.xm.qa.pages.EconomicCalendarPage;
import com.xm.qa.util.TestUtil;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class EconomicCalendarPageTest extends TestBase {
    EconomicCalendarPage economicCalendarPage;

    public EconomicCalendarPageTest() {
        super();
    }

    @BeforeMethod (alwaysRun = true)
    public void setUp() {
        initialization(prop.getProperty("urlEconomicCalendar"));
        economicCalendarPage = new EconomicCalendarPage();
    }

    @Test
    public void economicCalendarPageYesterdayTest() throws InterruptedException {
        String yesterdayDateActual = economicCalendarPage.clickYesterdayButtonAndGetDate();
        String yesterdayDateExpected = TestUtil.yesterday();
        Assert.assertEquals(yesterdayDateActual,yesterdayDateExpected);
    }

    @Test
    public void economicCalendarPageTodayTest() throws InterruptedException {
        String todayDateActual = economicCalendarPage.clickTodayButtonAndGatDate();
        String todayDateExpected = TestUtil.today();
        Assert.assertEquals(todayDateActual,todayDateExpected);
    }

    @Test
    public void economicCalendarPageTomorrowTest() throws InterruptedException {
        String tomorrowDateActual = economicCalendarPage.clickTomorrowButtonAndGatDate();
        String tomorrowDateExpected = TestUtil.tomorrow();
        Assert.assertEquals(tomorrowDateActual,tomorrowDateExpected);
    }

    @Test
    public void economicCalendarPageThisWeekTest() throws InterruptedException {
        String thisWeekDatesActual = economicCalendarPage.clickThisWeekButton();
        String thisWeekDatesExpected = TestUtil.thisWeek();
        Assert.assertEquals(thisWeekDatesActual,thisWeekDatesExpected);
    }

    @Test
    public void validateDisclaimerHeader(){
        String actualDisclaimerHeader = economicCalendarPage.clickHereButtonInDisclaimer();
        String expectedDisclaimerHeader = "Notification on Non-Independent Investment Research and Risk Warning";
        Assert.assertEquals(actualDisclaimerHeader,expectedDisclaimerHeader);
    }

    @AfterMethod (alwaysRun = true)
    public void tearDown() {
        driver.close();
        driver.quit();
    }
}
