package com.xm.qa.testcases;

import com.xm.qa.base.TestBase;
import com.xm.qa.pages.RiskWarningPage;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.io.IOException;

public class RiskAndWarningPageTest extends TestBase {

    RiskWarningPage riskWarningPage;

    public RiskAndWarningPageTest(){
        super();
    }

    @BeforeMethod
    public void setUp(){
        initialization(prop.getProperty("urlRiskWarningPage"));
        riskWarningPage = new RiskWarningPage();
    }

    @Test
    public void validateDisclaimerPdf() throws IOException, InterruptedException {
        String disclaimerPdfTextActual = riskWarningPage.riskWarningDisclaimerPdfContent();
        String disclaimerHeaderExpected = "Address: 12, Richard & Verengaria Street, Araouzos Castle Court, 3042, P.O.Box 50626, 3608 Limassol, Cyprus \n" +
                "RISK DISCLOSURE \n" +
                " \n" +
                "INTRODUCTION ";
        Assert.assertTrue(disclaimerPdfTextActual.contains(disclaimerHeaderExpected));
    }

    @AfterMethod
    public void tearDown(){
        driver.close();
        driver.quit();
    }
}
