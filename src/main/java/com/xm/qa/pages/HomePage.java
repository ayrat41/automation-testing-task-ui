package com.xm.qa.pages;

import com.xm.qa.base.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class HomePage extends TestBase {

    @FindBy(xpath = "//*[@id=\"main-nav\"]/li[4]/a")
    WebElement researchAndEducation;

    @FindBy(id = "main-nav")
    WebElement listOfLinksOnMainMenu;

    @FindBy(xpath = "//div[@id='researchMenu']/ul[contains(@class, 'list')]")
    List<WebElement> listOfLinksOnResearchMenu;

    @FindBy(xpath = "//*[@id=\"main-nav\"]/li[4]/div/div/div[3]/div[1]/ul/li[6]/a")
    WebElement economicCalendarLink;

    @FindBy(xpath = "(//img[contains(@alt, 'Logo')])[2]")
    WebElement homePageLogo;

    @FindBy(xpath = "//*[@id=\"main-nav\"]/li[4]/div/div/div[1]")
    WebElement researchAndEducationValidator;

    public HomePage(){
        PageFactory.initElements(driver, this);
    }

    public String validateHomePageTitle(){
        return driver.getTitle();
    }

    public boolean validateHomePageLogo(){
        return homePageLogo.isDisplayed();
    }

    public boolean researchAndEducationTextIsDisplayed(){
        researchAndEducation.click();
        return researchAndEducationValidator.isDisplayed();
    }
}
