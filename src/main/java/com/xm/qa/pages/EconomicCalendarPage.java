package com.xm.qa.pages;

import com.xm.qa.base.TestBase;
import com.xm.qa.util.TestUtil;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class EconomicCalendarPage extends TestBase {

    @FindBy(id = "timeFrame_yesterday")
    WebElement yesterdayButton;

    @FindBy(id = "timeFrame_today")
    WebElement todayButton;

    @FindBy(id = "timeFrame_tomorrow")
    WebElement tomorrowButton;

    @FindBy(id = "timeFrame_thisWeek")
    WebElement thisWeekButton;

    @FindBy(xpath = "//*[@id=\"research-app\"]/div[4]/div/p[3]/a")
    WebElement hereLinkInDisclaimer;

    @FindBy(xpath = "//*[@id=\"widgetFieldDateRange\"]")
    WebElement fieldDateRange;

    @FindBy(xpath = "//*[@id=\"research-app\"]/div[3]/div/div[1]/div/div[2]/div/iframe")
    WebElement calendarFrame;

    @FindBy(xpath = "//*[@id=\"research-app\"]/div[3]/h2")
    WebElement disclaimerHeader;

    public EconomicCalendarPage(){
        PageFactory.initElements(driver, this);
    }

    public String validateEconomicCalendarTitle(){
        return driver.getTitle();
    }

    public String clickYesterdayButtonAndGetDate() throws InterruptedException {
        driver.switchTo().frame(calendarFrame);
        yesterdayButton.click();
        Thread.sleep(2000);
        return TestUtil.getSingleDate(fieldDateRange.getText());
    }
    public String clickTodayButtonAndGatDate()throws InterruptedException {
        driver.switchTo().frame(calendarFrame);
        todayButton.click();
        Thread.sleep(2000);
        return TestUtil.getSingleDate(fieldDateRange.getText());
    }
    public String clickTomorrowButtonAndGatDate() throws InterruptedException {
        driver.switchTo().frame(calendarFrame);
        tomorrowButton.click();
        Thread.sleep(2000);
        return TestUtil.getSingleDate(fieldDateRange.getText());
    }
    public String clickThisWeekButton() throws InterruptedException {
        driver.switchTo().frame(calendarFrame);
        thisWeekButton.click();
        Thread.sleep(2000);
        return fieldDateRange.getText();
    }
    public String clickHereButtonInDisclaimer(){
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].click()", hereLinkInDisclaimer);
        return disclaimerHeader.getText();
    }


}
