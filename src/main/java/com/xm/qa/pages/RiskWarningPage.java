package com.xm.qa.pages;

import com.xm.qa.base.TestBase;
import com.xm.qa.util.TestUtil;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;

public class RiskWarningPage extends TestBase {

    @FindBy(xpath = "//*[@id=\"research-app\"]/div[3]/p[11]/a")
    WebElement hereLinkOnRiskWarning;

    public RiskWarningPage(){
        PageFactory.initElements(driver, this);
    }

    public boolean hereLinkOnRiskWarning(){
        return  hereLinkOnRiskWarning.isEnabled();
    }

    public String riskWarningDisclaimerPdfContent() throws IOException, InterruptedException {
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].click()", hereLinkOnRiskWarning);
        Thread.sleep(3000);
        System.out.println(driver.getCurrentUrl());
        String pdfContent = TestUtil.readPrf(hereLinkOnRiskWarning.getAttribute("href"));
        System.out.println(pdfContent);
        return pdfContent;
    }

}
