package com.xm.qa.util;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class TestUtil {

    public static long PAGE_LOAD_TIMEOUT = 20;
    public static long IMPLICIT_WAIT = 10;
    public static DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    public static String yesterday() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);
        return dateFormat.format(calendar.getTime());
    }

    public static String today() {
        Calendar calendar = Calendar.getInstance();
        return dateFormat.format(calendar.getTime());
    }

    public static String tomorrow() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 1);
        return dateFormat.format(calendar.getTime());
    }

    public static String thisWeek() {
        Calendar c = GregorianCalendar.getInstance();
        c.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        String startDate = "", endDate = "";

        startDate = df.format(c.getTime());
        c.add(Calendar.DATE, 6);
        endDate = df.format(c.getTime());
        return startDate + " - " + endDate;
    }

    public static String getSingleDate(String dateRangeString) {
        String[] dateRange = dateRangeString.split("-");
        Optional<String> yesterday = Arrays.stream(dateRange).reduce((current, next) -> {
            if (current == next) System.out.println("Date " + next);
            return next.trim();
        });
        return yesterday.get();
    }

    public static String readPrf(String url) throws IOException {
        URL urlPdf = new URL(url);
        InputStream ip = urlPdf.openStream();
        BufferedInputStream bufferedInputStreamIp = new BufferedInputStream(ip);
        PDDocument pdDocument = PDDocument.load(bufferedInputStreamIp);
        String content = new PDFTextStripper().getText(pdDocument);
        return content;
    }
}
