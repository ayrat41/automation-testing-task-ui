package com.xm.qa.base;

import com.xm.qa.util.TestUtil;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class TestBase {
    public static WebDriver driver;
    public static Properties prop;
    public static String windowSize;

    public TestBase() {

        try {
            prop = new Properties();
            FileInputStream ip = new FileInputStream("src/main/resources/config.properties");
            prop.load(ip);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void initialization(String url) {
        String browserName = System.getProperty("browserOS");
        if (StringUtils.isBlank(browserName)) {
            browserName="chromeMac";
        }
        if (browserName.equals("chromeMac")) {
            System.setProperty("webdriver.chrome.driver", "src/main/java/com/xm/qa/config/chromedriverMacOs");
        } else if (browserName.equals("chromeWindows")) {
            System.setProperty("webdriver.chrome.driver", "src/main/java/com/xm/qa/config/chromedriverWindows.exe");
        } else {
            System.out.println("Set the browser in the property file");
        }
        driver = new ChromeDriver();
        if (StringUtils.isBlank(windowSize)) ;
        {
            System.setProperty("windowSize", "maximum");
        }
        windowSize = System.getProperty("windowSize");
        switch (windowSize) {
            case "medium": {
                Dimension dimension = new Dimension(1024, 768);
                driver.manage().window().setSize(dimension);
                break;
            }
            case "minimum": {
                Dimension dimension = new Dimension(800, 600);
                driver.manage().window().setSize(dimension);
                break;
            }
            case "maximum": {
                driver.manage().window().maximize();
                break;
            }
        }
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_WAIT, TimeUnit.SECONDS);
        driver.get(url);
        driver.findElement(By.xpath("//button[contains(text(), 'CONTINUE')]")).click();

    }
}
