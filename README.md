This is a set of automated test cases for fx.com

To install project use maven command: mvn clean install -DskipTests;

To run test provide screen resolution as maven parameter windowSize. Options available medium for 1024x768,minimum for 800x600 and maximum:
Ex: -DwindowSize=medium

To run test provide browserOS as maven parameter. Options available chromeWindows, chromeMacOs
Ex: -DbrowserOS=chromeMacOs

Chrome version supported: ChromeDriver 87.0.4280.88

Default parameters DwindowSize=maximum -DbrowserOS=chromeMacOs